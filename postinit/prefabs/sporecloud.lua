AddPrefabPostInit("sporecloud", function(inst)
    if GLOBAL.TheWorld.ismastersim then
        inst.components.aura.auratestfn = function(self, inst)
            if not inst.components.inventory or not inst.components.inventory.equipslots or not inst.components.inventory.equipslots.head or not inst.components.inventory.equipslots.head.prefab == "gashat" then
                return true
            end
        end
    end
end)