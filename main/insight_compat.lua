_G = GLOBAL

local function AddDescriptors()
    if not _G.rawget(_G, "Insight") then return end

    _G.Insight.descriptors.twisterspawner = _G.require("descriptors/islandadventures_twisterspawner")

end

AddSimPostInit(AddDescriptors) -- _G.Insight.descriptors may not exist yet, but it will exist at AddSimPostInit.